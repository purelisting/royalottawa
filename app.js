var express = require(`express`);
var morgan = require(`morgan`);

// //- Webpack Dev Middleware
// var webpack = require('webpack');
// var config = require('./webpack.dev');
// var compiler = webpack(config);

var app = express();
app.set('port', process.env.NODE_PORT || 3000);
app.set('ip', process.env.NODE_IP || 'localhost');
app.set('view engine', 'pug');
app.set('views', './views');

// //- Webpack Dev Middleware
// app.use(require('webpack-dev-middleware')(compiler, {
//   noInfo: true,
//   publicPath: config.output.publicPath
// }));
// app.use(require('webpack-hot-middleware')(compiler));

app.use(morgan(`dev`));
app.use(express.static(`./public`));

app.get(`*`, function(req, res){
  res.render(`index`);
});

app.listen(app.get(`port`), app.get(`ip`), function(){
  console.log('Application ip ' + app.get(`ip`) + ':' + app.get(`port`));
  console.log('worker ' + process.pid + 'started...');
});
