import React, {Component} from 'react';
import {Provider} from 'react-redux';

// import Components
import Nav from './Components/Nav/Nav';
import Page from './Components/Page/Page';
import MainCourse from './Components/MainCourse/MainCourse';
import Royal9 from './Components/Royal9/Royal9';
import PropertyMap from './Components/PropertyMap/PropertyMap';
import Preview from './Components/Preview/Preview';

// import Store
import Store from './Store/Store';

class App extends Component {
  render(){
    return (
      <Provider store={Store}>
        <div>
          <Nav />
          {this.props.children}
          <MainCourse />
          <Royal9 />
          <PropertyMap />
          <Preview />
        </div>
      </Provider>
    );
  }
}

export default App;
