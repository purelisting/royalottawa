import {createStore, combineReducers} from 'redux';

import Nav from './Nav';
import MainCourse from './MainCourse';
import Royal9 from './Royal9';
import Preview from './Preview';

const reducer = combineReducers({
  Nav: Nav,
  MainCourse: MainCourse,
  Royal9: Royal9,
  Preview: Preview
});

const Store = createStore(reducer);

export default Store;
