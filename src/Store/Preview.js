import {fromJS} from 'immutable';

export default (state = fromJS({
  open: false,
  pic: '/img/MainCourse/Hole1/1a.jpg'
}), action) => {
  switch (action.type) {
    case 'PREVIEW_TOGGLE':
      return state.update('open', open => !open);
    case 'PREVIEW_SET_PIC':
      return state.set('pic', action.pic);
    default:
      return state;
  }
};
