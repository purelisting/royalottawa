import {fromJS} from 'immutable';

export default (state = fromJS({
  mainCourse: {
    open: false,
    hover: false
  },
  royal9: {
    open: false,
    hover: false
  },
  propertyMap:{
    open: false,
    hover: false
  }
}), action)=>{
  switch (action.type) {
    case 'NAV_TOGGLE':
      return state.map((item, key)=>item.update('open', open => key === action.button && !open));
    case 'NAV_CLOSE_ALL':
      return state.map((item, key)=>item.set('open', false));
    case 'NAV_SET':
      return state.map((item, key)=>item.set('open', key === action.button && action.set));
    case 'NAV_HOVER_TOGGLE':
      return state.map((item, key)=>item.update('hover', hover => key === action.button && !hover));
    default:
      return state;
  }
};
