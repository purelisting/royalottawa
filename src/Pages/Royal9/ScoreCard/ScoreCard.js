import React, {Component} from 'react';
import {Link, withRouter} from 'react-router';
import {connect} from 'react-redux';

import Style from './ScoreCard.styl';
import ScoreListItem from '../../../Components/ScoreListItem/ScoreListItem';

class ScoreCard extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    const {data, router} = this.props;
    return (
      <div className={Style.ScoreCard}>
        <div className={`${Style.column} ${Style.Logo}`}>
          <div className={Style.logoWrapper}>
            <div className='imgWrapper'>
              <Link to='/' style={{display: 'block', margin: 'auto'}}><img src='/img/Logo.png' className={Style.img} /></Link>
            </div>
            <div className='ButtonWrapperWrapper'>
            </div>
          </div>
        </div>
        <div className={`${Style.column} ${Style.Score}`}>
          <div className={Style.ScoreWrapper}>
            <ScoreListItem
              border={true}
              number={'Hole'}
              par={'Par'}
              data={['White', 'Green', 'Gold']}
              numberStyle={{fontSize: 14, fontFamily: 'trajan-pro-3'}} />
            {
              data.map((score, index) => (
                <ScoreListItem key={index} border={true} par={score.get('par')} data={score.get('ScoreCard').toJS()} number={index+1} onClick={()=>{
                  router.push(`/Royal9/${index+1}`);
                }}/>
              ))
            }
            <ScoreListItem
              border={false}
              number='TOT'
              data={
                data.reduce((acc, item)=>{
                  const i = item.get('ScoreCard').toJS();
                  return [acc[0] + i[0] ,acc[1] + i[1] ,acc[2] + i[2]];
                }, [0,0,0])
              }
              par={
                data.reduce((acc, item) =>{
                  return acc + parseInt(item.get('par'));
                }, 0)
              }
              hover={false}
              onClick={()=>{}}
              onHover={()=>{}}
              offHover={()=>{}}
              numberStyle={{fontSize: 14}}/>
          </div>
        </div>
      </div>
    );
  }
};

const mapStateToProps = ({Royal9}) => {
  return {
    data: Royal9
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'royal9'
      });
    }
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ScoreCard));
