import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={5}
        name='Faraway'
        par={5}
        scoreCard={[480, 439, 394]}
        img='/img/Royal9/Hole5/Royal Hole 05.png'
        paragraph={`A straightforward tee shot from the elevated tee deck. Aim for the fairway bunker at the dogleg.  There is potential to reach the green in two. If not, you’ll have a short iron into a receptive back to front putting surface.`}
        previews={[
        ]}
        leftLink='/Royal9/4'
        rightLink='/Royal9/6'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'royal9'
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
