import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={4}
        name='Millenium'
        par={3}
        scoreCard={[136, 114, 90]}
        img='/img/Royal9/Hole4/Royal Hole 04.png'
        paragraph={`An accurate tee shot is required to hit this tiny green. Watch the crosswind; it can play havoc with your shot. `}
        previews={[
        ]}
        leftLink='/Royal9/3'
        rightLink='/Royal9/5'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'royal9'
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
