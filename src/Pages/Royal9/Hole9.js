import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={9}
        name='Homeward Bound'
        par={5}
        scoreCard={[455, 425, 375]}
        img='/img/Royal9/Hole9/Royal Hole 09.png'
        paragraph={`A straight tee shot is required to get you to the green in regulation. A right side approach is preferred. This severely contoured green can present some challenging putting. `}
        previews={[
        ]}
        leftLink='/Royal9/8'
        rightLink='/Royal9/1'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'royal9'
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
