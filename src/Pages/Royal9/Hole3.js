import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={3}
        name='The Oaks'
        par={4}
        scoreCard={[280, 275, 225]}
        img='/img/Royal9/Hole3/Royal Hole 03.png'
        paragraph={`Long hitters may be able to reach the green with their tee shots. If not, all you will need is a short iron into the elevated, narrow green. Left side approach is preferred. Good scoring opportunity. `}
        previews={[
        ]}
        leftLink='/Royal9/2'
        rightLink='/Royal9/4'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'royal9'
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
