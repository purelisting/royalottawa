import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={1}
        name='Over The Hill'
        par={4}
        scoreCard={[390, 345, 313]}
        img='/img/Royal9/Hole1/Royal Hole 01.png'
        paragraph={`A wide, uphill fairway with a generous entry to a narrow green. Don’t miss left on your approach. Be mindful of the subtle left to right slope when reading this green.`}
        previews={[
        ]}
        leftLink='/Royal9/9'
        rightLink='/Royal9/2'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'royal9'
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
