import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={2}
        name='Uphill'
        par='3'
        scoreCard={[199, 188, 182, 134]}
        img='/img/MainCourse/Hole2/Main Hole 02.png'
        paragraph='Club up for this uphill tee shot to a small and shallow green. Watch the top of the trees behind the green for wind direction. Short and in front of the green is not a bad option. '
        previews={[
          {
            pic: '/img/MainCourse/Hole2/2a.jpg',
            style: {
              top: `90%`
            }
          }
        ]}
        leftLink='/MainCourse/1'
        rightLink='/MainCourse/3'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'mainCourse'
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
