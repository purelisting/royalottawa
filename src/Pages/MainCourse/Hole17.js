import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={17}
        name='Hill Drive'
        par={4}
        scoreCard={[395, 341, 309, 274]}
        img='/img/MainCourse/Hole17/Main Hole 17.png'
        paragraph={`Play to the centre of the fairway on your tee shot avoiding the trees on the left for easy access to the green. The landing area below the hill leaves you with an uphill shot to a small, bunkered green. Check the top of the trees for wind direction on your second shot. `}
        previews={[
        ]}
        leftLink='/MainCourse/16'
        rightLink='/MainCourse/18'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'mainCourse'
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
