import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={5}
        name='Summit'
        par='4'
        scoreCard={[425, 382, 356, 272]}
        img='/img/MainCourse/Hole5/Main Hole 05.png'
        paragraph={`A tee shot a little right of centre on this left sloping fairway offers the safest entry to this elevated par 4. Wind direction is key for club selection. Beware of the deep bunkers on the left of this back to front sloping green. `}
        previews={[
        ]}
        leftLink='/MainCourse/4'
        rightLink='/MainCourse/6'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'mainCourse'
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
