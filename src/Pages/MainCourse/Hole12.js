import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={12}
        name='Bide-A-Wee'
        par={3}
        scoreCard={[152, 145, 129, 120]}
        img='/img/MainCourse/Hole12/Main Hole 12.png'
        paragraph={`Watch the wind from this elevated tee, as wind direction is critical to landing on this crowned, slick, putting surface. Putting is treacherous in all directions.`}
        previews={[
        ]}
        leftLink='/MainCourse/11'
        rightLink='/MainCourse/13'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'mainCourse'
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
