import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={13}
        name='Thirteen'
        par={4}
        scoreCard={[491, 440, 355, 301]}
        img='/img/MainCourse/Hole13/Main Hole 13.png'
        paragraph={`Take advantage of this generous uphill fairway. Your second shot will require an extra club if you haven't reached the flat with your tee shot. The green is large and receptive, but beware the subtle back to front slope.`}
        previews={[
        ]}
        leftLink='/MainCourse/12'
        rightLink='/MainCourse/14'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'mainCourse'
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
