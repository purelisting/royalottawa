import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={11}
        name='Little Misery'
        par={3}
        scoreCard={[165, 154, 135, 95]}
        img='/img/MainCourse/Hole11/Main Hole 11.png'
        paragraph={`The first of back-to-back par 3s, placement on the green is all-important for this hole. Stay below the flag for an uphill putt on this severely sloping green and be satisfied to escape with par.`}
        previews={[
        ]}
        leftLink='/MainCourse/10'
        rightLink='/MainCourse/12'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'mainCourse'
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
