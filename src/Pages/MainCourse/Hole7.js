import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={7}
        name='Peninsula'
        par={4}
        scoreCard={[347, 335, 317, 307]}
        img='/img/MainCourse/Hole7/Main Hole 07.png'
        paragraph={`"Aim for the willow." Driver may not be required on this short, but challenging par 4. A second shot landing a few of feet short of this front to back sloping green will prevent your approach shot from rolling through to the fringe behind the green.`}
        previews={[
        ]}
        leftLink='/MainCourse/6'
        rightLink='/MainCourse/8'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'mainCourse'
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
