import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={3}
        name='Woodside'
        par='4'
        scoreCard={[421, 417, 293, 293]}
        img='/img/MainCourse/Hole3/Main Hole 03.png'
        paragraph={`A large landing area before the gully near the 200 yard marker leaves a lengthy second shot into a generous, yet treacherous, elevated putting surface. Don't go long. If your tee shot puts you in the gully you'll need to club up for a blind shot to the green. `}
        previews={[
        ]}
        leftLink='/MainCourse/2'
        rightLink='/MainCourse/4'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'mainCourse'
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
