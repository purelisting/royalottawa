import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={9}
        name='Halfway'
        par={4}
        scoreCard={[448, 412, 394, 366]}
        img='/img/MainCourse/Hole9/Main Hole 09.png'
        paragraph={`A tee shot on the right, short of the ravine, leaves a rolling entry into this right to left sloping green. Long hitters can place their tee shot left to the ravine landing area for a rewarding approach. If playing as a 5, keep your second shot to the middle for a safe short iron in. `}
        previews={[
        ]}
        leftLink='/MainCourse/8'
        rightLink='/MainCourse/10'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'mainCourse'
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
