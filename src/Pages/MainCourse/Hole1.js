import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={1}
        name='St. Andrews'
        par={'5'}
        scoreCard={[488, 462, 436, 397]}
        img='/img/MainCourse/Hole1/Main Hole 01.png'
        paragraph='The opening hole offers a rolling fairway with a deep drop on the right hand side. A straight tee shot sets you up for a safe second shot to the "flat" which will leave a short iron into a small, flat green. Avoid long or left on your approach.'
        previews={[
          {
            pic: '/img/MainCourse/Hole1/1a.jpg',
            style: {
              top: `18%`,
              left: '48%'
            }
          },
          {
            pic: '/img/MainCourse/Hole1/1b.jpg',
            style: {
              top: `90%`,
              left: `60%`
            }
          }
        ]}
        leftLink='/MainCourse/18'
        rightLink='/MainCourse/2'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'mainCourse'
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
