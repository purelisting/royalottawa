import React, {Component} from 'react';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';

import Style from './ScoreCard.styl';
import ScoreListItem from '../../../Components/ScoreListItem/ScoreListItem';
import ScoreCardButton from '../../../Components/ScoreCardButton/ScoreCardButton';

class ScoreCard extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    const {data, router} = this.props;
    return (
      <div className={Style.ScoreCard}>
        <div className={`${Style.column} ${Style.Logo}`}>
          <div className={Style.logoWrapper}>
            <div className='imgWrapper'>
              <Link to='/' style={{display: 'block', margin: 'auto'}}><img src='/img/Logo.png' className={Style.img} /></Link>
            </div>
            <div className='ButtonWrapperWrapper'>
            </div>
          </div>
        </div>
        <div className={`${Style.column} ${Style.Score}`}>
          <div className={Style.ScoreWrapper}>
            <ScoreListItem
              border={true}
              number={'Hole'}
              par={'Par'}
              data={['Black', 'White', 'Green', 'Gold']}
              numberStyle={{fontSize: 14, fontFamily: 'trajan-pro-3'}} />
            {
              data.slice(0,9).map((score, index) => (
                <ScoreListItem key={index} border={true} par={score.get('par')} data={score.get('ScoreCard').toJS()} number={index+1} onClick={()=>{
                  router.push(`/MainCourse/${index+1}`);
                }} />
              ))
            }
            <ScoreListItem
              border={false}
              number='OUT'
              data={
                data.slice(0,9).reduce((acc, item)=>{
                  const i = item.get('ScoreCard').toJS();
                  return [acc[0] + i[0] ,acc[1] + i[1] ,acc[2] + i[2] ,acc[3] + i[3] ];
                }, [0,0,0,0])
              }
              par={
                data.slice(0,9).reduce((acc, item) =>{
                  return acc + parseInt(item.get('par'));
                }, 0)
              }
              hover={false}
              onClick={()=>{}}
              onHover={()=>{}}
              offHover={()=>{}}
              numberStyle={{fontSize: 14}}/>
            <ScoreListItem
              border={false}
              number=''
              data={['', '', '', '']}
              hover={false}
              onClick={()=>{}}
              onHover={()=>{}}
              offHover={()=>{}}/>
            {
              data.slice(9,18).map((score, index) => (
                <ScoreListItem
                  key={index}
                  border={true}
                  par={score.get('par')}
                  data={score.get('ScoreCard').toJS()}
                  number={index+10}
                  onClick={()=>{
                    router.push(`/MainCourse/${index+10}`);
                  }}
                  numberStyle={index === 1 ? {letterSpacing: '3px'} : {} }/>
              ))
            }
            <ScoreListItem
              border={true}
              number='IN'
              data={
                data.slice(9,18).reduce((acc, item)=>{
                  const i = item.get('ScoreCard').toJS();
                  return [acc[0] + i[0] ,acc[1] + i[1] ,acc[2] + i[2] ,acc[3] + i[3] ];
                }, [0,0,0,0])
              }
              par={
                data.slice(9,18).reduce((acc, item) =>{
                  return acc + parseInt(item.get('par'));
                }, 0)
              }
              hover={false}
              onClick={()=>{}}
              onHover={()=>{}}
              offHover={()=>{}}
              numberStyle={{fontSize: 14}}/>
            <ScoreListItem
              border={false}
              number='TOT'
              data={
                data.reduce((acc, item)=>{
                  const i = item.get('ScoreCard').toJS();
                  return [acc[0] + i[0] ,acc[1] + i[1] ,acc[2] + i[2] ,acc[3] + i[3] ];
                }, [0,0,0,0])
              }
              par={
                data.reduce((acc, item) =>{
                  return acc + parseInt(item.get('par'));
                }, 0)
              }
              hover={false}
              onClick={()=>{}}
              onHover={()=>{}}
              offHover={()=>{}}
              numberStyle={{fontSize: 14}}/>
          </div>
        </div>
      </div>
    );
  }
};

const mapStateToProps = ({MainCourse}) => {
  return {
    data: MainCourse
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'mainCourse'
      });
    }
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ScoreCard));
