import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={16}
        name='Mound'
        par={4}
        scoreCard={[384, 364, 339, 276]}
        img='/img/MainCourse/Hole16/Main Hole 16.png'
        paragraph={`Middle to left placement of your tee shot offers a better approach into this well-bunkered, elevated green. Be happy if your shot lands short and in the throat. Beware the pull of the river when putting.  `}
        previews={[
        ]}
        leftLink='/MainCourse/15'
        rightLink='/MainCourse/17'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'mainCourse'
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
