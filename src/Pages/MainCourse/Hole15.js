import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={15}
        name='Westward Ho'
        par={4}
        scoreCard={[438, 427, 422, 398]}
        img='/img/MainCourse/Hole15/Main Hole 15.png'
        paragraph={`A tee shot down the left side of this undulating fairway sets you up for a long fairway shot to the second half of this split fairway. Make use of the generous entrance to one of our larger greens. There is a limited roll if you're short of the green on your approach.  `}
        previews={[
        ]}
        leftLink='/MainCourse/14'
        rightLink='/MainCourse/16'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'mainCourse'
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
