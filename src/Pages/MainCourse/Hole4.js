import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={4}
        name='Laurentian'
        par='5'
        scoreCard={[522, 484, 453, 425]}
        img='/img/MainCourse/Hole4/Main Hole 04.png'
        paragraph={`A challenging double dogleg; play your tee shot towards the right side of the second fairway bunker on the left. A well-placed second shot, just short of the next set of bunkers, opens up the large, deep green. `}
        previews={[
        ]}
        leftLink='/MainCourse/3'
        rightLink='/MainCourse/5'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'mainCourse'
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
