import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Components
import Page from '../../Components/Page/Page';

class Home extends Component {
  componentDidMount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  componentWillUnmount(){
    const {toggleHoverMainCourse} = this.props;
    toggleHoverMainCourse();
  }
  render(){
    // console.log('HomePage');
    return (
      <Page
        number={18}
        name='Home'
        par={5}
        scoreCard={[573, 528, 465, 385]}
        img='/img/MainCourse/Hole18/Main Hole 18.png'
        paragraph={`No trouble until you're in front of the green. A long tee shot lets you choose your preferred distance for a "full swing" approach into this tricky back to front sloped, elevated green.  Avoid going long as recovery shots are treacherous.  `}
        previews={[
        ]}
        leftLink='/MainCourse/17'
        rightLink='/MainCourse/1'
        />
    );
  }
}

const mapStateToProps = ({Nav}) => {
  // console.log(Nav.toJS());
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    togglePropertyMap(){
      // console.log('toggle');
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    },
    toggleHoverMainCourse(){
      dispatch({
        type: 'NAV_HOVER_TOGGLE',
        button: 'mainCourse'
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
