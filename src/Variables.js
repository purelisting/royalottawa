import Color from 'color';

module.exports = {
  font: {
    main: 'proxima-nova',
    head: 'trajan-pro-3'
  },
  center:{
    display: 'block',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
  color: {
    black: '#000',
    white: '#FFF',
    green: '#008349',
    gold: '#E5AA2D',
    hover: '#DDC272'
  }
};
