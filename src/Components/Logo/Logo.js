import React, {Component} from 'react';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';

// import Styles
import Style from './Logo.styl';

class Logo extends Component {
  render(){
    const {router, propertyMapOn, style} = this.props;
    return (
      <div className={Style.Logo} style={style}>
        <img src='/img/Logo.png' onClick={()=>{
          router.push('/');
          propertyMapOn();
        }}/>
      </div>
    );
  }
}

const mapStateToProps = ({Nav}) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    propertyMapOn(){
      dispatch({type: 'NAV_SET', button: 'propertyMap', set: true});
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Logo));
