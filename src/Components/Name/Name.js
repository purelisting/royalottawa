import React, {Component} from 'react';

// import Styles
import Style from './Name.styl';

class Name extends Component {
  constructor(props){
    super(props);
  }
  render(){
    const {number, name, par} = this.props;
    return (
      <div className={Style.Name}>
        <h1 className={Style.head}>
          <div className={Style.number}>
            {number}
          </div>
          <br />
          <div className={Style.name}>
            {name}
          </div>
          <br />
          <div className={Style.par}>
            Par {par}
          </div>
        </h1>
      </div>
    );
  }
}

Name.defaultProps = {
  number: 0,
  name: 'name',
  par: '0'
};

export default Name;
