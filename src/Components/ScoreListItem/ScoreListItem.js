import React, {Component} from 'react';
import Radium, {StyleRoot} from 'radium';

// import Styles
// import Style from './Style'
import Stylus from './ScoreListItem.styl'

class ScoreListItem extends Component {
  render(){
    const {border, number, par, numberStyle, data, hover, onClick, onHover, offHover} = this.props;
    if(data.length === 4)
      return (
        <div className={`${Stylus.ScoreListItem} ${!border && Stylus.noBorder} ${data.length === 4 && Stylus.mainCourse} ${data.length === 3 && Stylus.royal9} ${hover && Stylus.hover}`}
          onClick={onClick}
          onMouseOver={onHover}
          onMouseOut={offHover}>
          <div className={`column number`} style={numberStyle}>
            {number}
          </div>
          <div className={`column ${Stylus.black}`} style={numberStyle ? numberStyle : {}}>
            {data[0]}
          </div>
          <div className={`column ${Stylus.white}`} style={numberStyle ? numberStyle : {}}>
            {data[1]}
          </div>
          <div className={`column ${Stylus.green}`} style={numberStyle ? numberStyle : {}}>
            {data[2]}
          </div>
          <div className={`column ${Stylus.gold}`} style={numberStyle ? numberStyle : {}}>
            {data[3]}
          </div>
          <div className={`column number ${Stylus.par}`} style={numberStyle ? numberStyle : {}}>
            {par}
          </div>
        </div>
      );
    return (
      <div className={`${Stylus.ScoreListItem} ${!border && Stylus.noBorder} ${data.length === 4 && Stylus.mainCourse} ${data.length === 3 && Stylus.royal9} ${hover && Stylus.hover}`}
        onClick={onClick}
        onMouseOver={onHover}
        onMouseOut={offHover}>
        <div className={`column number`} style={numberStyle}>
          {number}
        </div>
        <div className={`column ${Stylus.white}`} style={numberStyle ? numberStyle : {}}>
          {data[0]}
        </div>
        <div className={`column ${Stylus.green}`} style={numberStyle ? numberStyle : {}}>
          {data[1]}
        </div>
        <div className={`column ${Stylus.gold}`} style={numberStyle ? numberStyle : {}}>
          {data[2]}
        </div>
        <div className={`column number ${Stylus.par}`} style={numberStyle ? numberStyle : {}}>
          {par}
        </div>
      </div>
    );
  }
}

export default Radium(ScoreListItem);
