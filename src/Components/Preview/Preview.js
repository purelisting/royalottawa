import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Motion, spring, presets} from 'react-motion';

//- import Styles
import Style from './Preview.styl'

class Preview extends Component {
  render(){
    const {open, src, toggle} = this.props;
    return (
      <Motion style={{
        size: open ? spring(100, presets.gentle) : spring(0, presets.gentle),
        opacity: open ? spring(1, {stiffness: 120, damping: 20}) : spring(0, {stiffness: 120, damping: 20})
      }}>
        {
          ({size, opacity}) => (
            <div className={Style.base} onClick={toggle} style={{
              transform: `translate(0%, ${size}%)`,
              opacity: opacity
            }}>
              <div className={Style.imgWrapper} >
                { src !== '' && <img src={src} alt='Hole Graphic' /> }
              </div>
            </div>
          )
        }
      </Motion>
    );
  }
};

const mapStateToProps = ({Preview}) => {
  return {
    open: Preview.get('open'),
    src: Preview.get('pic')
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    toggle(){
      dispatch({type: 'PREVIEW_TOGGLE'});
      dispatch({type: 'PREVIEW_SET_PIC', pic: ''});
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Preview);
