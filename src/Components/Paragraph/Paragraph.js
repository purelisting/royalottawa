import React, {Component} from 'react';
import Radium, {StyleRoot} from 'radium';

// import Styles
import Style from './Paragraph.styl';

class Paragraph extends Component {
  render(){
    const {paragraph} = this.props;
    return (
      <div className={Style.Paragraph}>
          {paragraph}
      </div>
    );
  }
}

Paragraph.defaultProps = {
  paragraph : 'Paragraph'
};

Paragraph.Proptypes = {
  paragraph: React.PropTypes.string
};

export default Radium(Paragraph);
