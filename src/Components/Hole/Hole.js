import React, {Component} from 'react';
import {connect} from 'react-redux';

// import Styles
import Style from './Hole.styl';

class Hole extends Component {
  render(){
    const {src, previews, openPreview} = this.props;
    return (
      <div className={Style.Hole}>
        <img src={src} className={Style.img} />
        {
          previews.map(({pic, style}) => (
            <i className='btr bt-camera' key={pic} onClick={openPreview(pic)} style={style} />
          ))
        }
      </div>
    );
  }
}

const mapStateToProps = ({Preview}) => {
  return {
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    openPreview(img){
      return () => {
        dispatch({type: 'PREVIEW_SET_PIC', pic: img});
        dispatch({type: 'PREVIEW_TOGGLE'});
      };
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Hole);
