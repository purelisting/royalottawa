import React from 'react';
import Radium from 'radium';
import Color from 'color';

const Path = ({d, onClick, hover, onHover, offHover})=>{
  return (
    <path
      d={d}
      onClick={onClick}
      style={[{
          fill: `transparent`,
          stroke: `transparent`,
          cursor: `pointer`
        }, hover && {
          fill: Color(`#DDC272`).clearer(0.5).rgbString()
        }]}
      onMouseOver={onHover}
      onMouseOut={offHover}/>
  );
};

export default Radium(Path);
