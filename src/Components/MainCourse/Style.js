import Color from 'color';
import {font, color} from '../../Variables';

const {white} = color;

// console.log(color.white);

export default {
  base:{
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    paddingTop: 70,
    overflow: 'hidden'
  },
  mobileColumn:{
    width:'16.66%',
    margin: 'auto',
    color: Color(white).rgbString(),
    borderRight: `1px solid ${Color(white).rgbString()}`,
    textAlign: 'center',
    lineHeight: '40px',
    fontSize: 20
  },
  column:{
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column'
  },
  wrapper: {
    maxWidth: 1440,
    margin: 'auto'
  },
  imgWrapper:{
    maxWidth: 600,
    maxHeight: 698.47,
    position: 'relative',
    margin: 'auto'
  },
  SVG:{
    maxWidth: '100%',
    maxHeight: '100%'
  },
  img:{
    maxWidth: '100%',
    maxHeight: '100%',
    margin: 'auto'
  }
};
