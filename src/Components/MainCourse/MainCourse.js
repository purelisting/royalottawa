import React, {Component} from 'react';
import Color from 'color';
import {color} from '../../Variables';
import {connect} from 'react-redux';
import Radium, {StyleRoot} from 'radium';
import MediaQuery from 'react-responsive';
import {Motion, spring, presets} from 'react-motion';
import {withRouter} from 'react-router';
import {Link} from 'react-router';

import ScoreListItem from '../ScoreListItem/ScoreListItem';
import SVG from '../SVG/SVG';
import Hand from '../Hand/Hand';
import Logo from '../Logo/Logo';
import ScoreCardButton from '../ScoreCardButton/ScoreCardButton';

import Style from './Style';
import Stylus from './MainCourse.styl';

class MainCourse extends Component {
  render(){
    const {active, mobileMap, mainCourse, first, rest, toggleHover, navToggle, router, closeNav} = this.props;
    return (
      <Motion style={{
          Y: active ? spring(0, presets.gentle) : spring(-100, presets.gentle)
        }}>
        {
          ({Y}) => (
            <div className={Stylus.MainCourse} style={{
              transform: `translate(0%, ${Y}%)`
            }}>
              <MediaQuery maxDeviceWidth={767}>
                <Motion style={{
                  Y: active ? spring(0, presets.gentle) : spring(-100, presets.gentle)
                }}>
                  {
                    ({Y}) => (
                      <div style={{
                        transform: `translate(0%, ${Y}%)`,
                        width: '100%',
                        height: 270,
                        position: 'fixed',
                        top: 0,
                        left: 0,
                        paddingTop: 50,
                        backgroundColor: Color(color.black).clearer(0.25).rgbString()
                      }}>
                        <div>
                          <div style={{
                            display: 'block',
                            borderBottom: `1px solid ${Color(color.white).rgbString()}`,
                            width: '100%',
                            height: 60,
                            color: Color(color.white).rgbString()
                          }}>
                            <div style={{
                              float: 'left',
                              width: '75%',
                              height: 40,
                              borderRight: `1px solid ${Color(color.white).rgbString()}`,
                              textAlign: 'center',
                              margin: '10px auto',
                              padding: '10px 0px'
                            }} onClick={()=>{
                              router.push(`/MainCourse/ScoreCard`);
                              navToggle('mainCourse');
                            }}>
                              <div style={{
                                fontSize: '16px',
                                fontFamily: 'trajan-pro-3',
                                fontWeight: 400,
                                letterSpacing: '3px'
                              }}>
                                SCORE CARD
                              </div>
                            </div>
                            <div style={{
                              float: 'left',
                              width: '25%',
                              height: 40,
                              textAlign: 'center',
                              margin: '10px auto',
                              padding: '10px 0px'
                            }} onClick={()=>{
                              navToggle('mainCourse');
                            }}>
                              <i className='btr bt-times' style={{
                                fontSize: 20
                              }}/>
                            </div>
                          </div>
                        </div>
                        <div style={{
                          padding: '20px 0px',
                          display: 'block',
                          width: '100%',
                          maxWidth: 300,
                          margin: 'auto'
                        }}>
                          {
                            mainCourse.map((hole, index) => (
                              <div className={Stylus.mobileColumn} key={index} onClick={()=>{
                                router.push(`/MainCourse/${index+1}`);
                                closeNav();
                              }} style={[ ((index+1)%6 === 0) && {borderRight: 'none'} ]}>
                                {index+1}
                              </div>
                            ))
                          }
                        </div>
                      </div>
                    )
                  }
                </Motion>
              </MediaQuery>
              <MediaQuery minDeviceWidth={768} maxDeviceWidth={1024} orientation='landscape'>
                <div className={Stylus.wrapper}>
                  <div className={Stylus.Left}>
                    <div className={Stylus.LeftContainer}>
                      <Logo style={{
                        width: 150,
                        margin: '50px auto'
                      }} />
                      <Link id='button' className={Style.Button} to='/MainCourse/ScoreCard' style={{
                      }} onClick={()=>{
                        closeNav();
                      }}>
                        SCORE CARD
                      </Link>
                      <Hand text='SELECT <br/> HOLE' style={{
                        width: 150,
                        margin: '50px auto'
                      }} />
                    </div>
                  </div>
                  <div className={Stylus.Right}>
                    <div className={Stylus.imgWrapper} style={{
                      maxWidth: 513,
                      maxHeight: 600
                    }}>
                      <img className={Stylus.img} src='/img/MainCourse/MainCourse.png' />
                      <SVG path={mainCourse} viewBox='0 0 719 837' toggleHover={toggleHover} linkTo='MainCourse'/>
                    </div>
                  </div>
                </div>
              </MediaQuery>
              <MediaQuery minDeviceWidth={768} maxDeviceWidth={1024} orientation='portrait'>
                <div className={Stylus.wrapper}>
                  <div className={Stylus.Left}>
                    <div className={Stylus.LeftContainer}>
                      <div className={Stylus.oneThird}>
                        <Logo style={{
                          position: 'absolute',
                          top: '50%',
                          left: '50%',
                          transform: 'translate(-50%, -50%)',
                          width: 150,
                          margin: 'auto'
                        }} />
                      </div>
                      <div className={Stylus.oneThird}>
                        <Link id='button' className={Style.Button} to='/MainCourse/ScoreCard' style={{
                          position: 'absolute',
                          top: '50%',
                          left: '50%',
                          transform: 'translate(-50%, -50%)'
                        }} onClick={()=>{
                          closeNav();
                        }}>
                          SCORE CARD
                        </Link>
                      </div>
                      <div className={Stylus.oneThird}>
                        <Hand text='SELECT <br/> HOLE' style={{
                          position: 'absolute',
                          top: '50%',
                          left: '50%',
                          transform: 'translate(-50%, -50%)',
                          width: 150
                        }} />
                      </div>
                    </div>
                  </div>
                  <div className={Stylus.Right}>
                    <div className={Stylus.imgWrapper} style={{
                      maxWidth: 513,
                      maxHeight: 600
                    }}>
                      <img className={Stylus.img} src='/img/MainCourse/MainCourse.png' />
                      <SVG path={mainCourse} viewBox='0 0 719 837' toggleHover={toggleHover} linkTo='MainCourse'/>
                    </div>
                  </div>
                </div>
              </MediaQuery>
              <MediaQuery minDeviceWidth={1025}>
                <div className={Stylus.wrapper}>
                  <div className={`${Stylus.column} ${Stylus.ScoreList}`}>
                    <StackedList {...this.props} />
                  </div>
                  <div className={`${Stylus.column} ${Stylus.Map}`}>
                    <div className={Stylus.imgWrapper}>
                      <img className={Stylus.img} src='/img/MainCourse/MainCourse.png' />
                      <SVG path={mainCourse} viewBox='0 0 719 837' toggleHover={toggleHover} linkTo='MainCourse'/>
                      <Logo style={{
                        position: 'absolute',
                        bottom: '21%',
                        left: '-15%',
                        width: 150
                      }} />
                      <Hand text='SELECT <br/> HOLE' style={{
                        position: 'absolute',
                        bottom: '5%',
                        left: '-10%',
                        width: 150
                      }} />
                    </div>
                  </div>
                </div>
              </MediaQuery>
            </div>
          )
        }
      </Motion>
    );
  }
};

class StackedList extends Component {
  render(){
    const {active, mainCourse, first, rest, toggleHover, router, closeNav} = this.props;
    return (
      <div className={Stylus.StackedList}>
        <ScoreListItem
          border={true}
          number={'Hole'}
          par={'Par'}
          data={['Black', 'White', 'Green', 'Gold']}
          numberStyle={{fontSize: 14, fontFamily: 'trajan-pro-3'}} />
        {
          first.map((item, index) => (
            <ScoreListItem
              border={true}
              number={index + 1}
              par={item.get('par')}
              data={item.get('ScoreCard').toJS()}
              key={index}
              hover={item.get('hover')}
              onClick={()=>{
                router.push(`/MainCourse/${index+1}`);
                closeNav();
              }}
              onHover={toggleHover(index)}
              offHover={toggleHover(index)}/>
          ))
        }
        <ScoreListItem
          border={false}
          number='OUT'
          data={
            first.reduce((acc, item)=>{
              const i = item.get('ScoreCard').toJS();
              return [acc[0] + i[0] ,acc[1] + i[1] ,acc[2] + i[2] ,acc[3] + i[3] ];
            }, [0,0,0,0])
          }
          par={
            first.reduce((acc, item) =>{
              return acc + parseInt(item.get('par'));
            }, 0)
          }
          hover={false}
          onClick={()=>{}}
          onHover={()=>{}}
          offHover={()=>{}}
          numberStyle={{fontSize: 14, fontFamily: 'trajan-pro-3'}} />
        <ScoreListItem
          border={false}
          number=''
          data={[' ', ' ', ' ', ' ']}
          hover={false}
          onClick={()=>{}}
          onHover={()=>{}}
          offHover={()=>{}}/>
        {
          rest.map((item, index) => (
            <ScoreListItem
              border={true}
              number={index + 10}
              par={item.get('par')}
              data={item.get('ScoreCard').toJS()}
              key={index}
              hover={item.get('hover')}
              onClick={()=>{
                router.push(`/MainCourse/${index+10}`);
                closeNav();
              }}
              onHover={toggleHover(index+9)}
              offHover={toggleHover(index+9)}
              numberStyle={index === 1 ? {letterSpacing: '3px'} : {} }/>
          ))
        }
        <ScoreListItem
          border={true}
          number='IN'
          par={
            rest.reduce((acc, item) =>{
              return acc + parseInt(item.get('par'));
            }, 0)
          }
          data={
            rest.reduce((acc, item)=>{
              const i = item.get('ScoreCard').toJS();
              return [acc[0] + i[0] ,acc[1] + i[1] ,acc[2] + i[2] ,acc[3] + i[3] ];
            }, [0,0,0,0])
          }
          hover={false}
          onClick={()=>{}}
          onHover={()=>{}}
          offHover={()=>{}}
          numberStyle={{fontSize: 14, fontFamily: 'trajan-pro-3'}} />
        <ScoreListItem
          border={false}
          number='TOT'
          data={
            mainCourse.reduce((acc, item)=>{
              const i = item.get('ScoreCard').toJS();
              return [acc[0] + i[0] ,acc[1] + i[1] ,acc[2] + i[2] ,acc[3] + i[3] ];
            }, [0,0,0,0])
          }
          par={
            mainCourse.reduce((acc, item) =>{
              return acc + parseInt(item.get('par'));
            }, 0)
          }
          hover={false}
          onClick={()=>{}}
          onHover={()=>{}}
          offHover={()=>{}}
          numberStyle={{fontSize: 14, fontFamily: 'trajan-pro-3'}} />
      </div>
    );
  }
};

class UnStackedList extends Component {
  render(){
    const {active, mainCourse, first, rest, toggleHover, router, closeNav} = this.props;
    return (
      <div className={Stylus.wrapper}>
        <div className={Stylus.unStackListColumn}>
          {
            first.map((item, index) => (
              <ScoreListItem
                border={true}
                number={index + 1}
                data={item.get('ScoreCard').toJS()}
                key={index}
                hover={item.get('hover')}
                onClick={()=>{
                  router.push(`/MainCourse/${index+1}`);
                  closeNav();
                }}
                onHover={toggleHover(index)}
                offHover={toggleHover(index)}/>
            ))
          }
          <ScoreListItem
            border={false}
            number='OUT'
            data={
              first.reduce((acc, item)=>{
                const i = item.get('ScoreCard').toJS();
                return [acc[0] + i[0] ,acc[1] + i[1] ,acc[2] + i[2] ,acc[3] + i[3] ];
              }, [0,0,0,0])
            }
            par={
              first.reduce((acc, item) =>{
                return acc + parseInt(item.get('par'));
              }, 0)
            }
            hover={false}
            onClick={()=>{}}
            onHover={()=>{}}
            offHover={()=>{}}
            numberStyle={{fontSize: 14}}/>
          <ScoreListItem
            border={false}
            number=''
            data={['', '', '', '']}
            hover={false}
            onClick={()=>{}}
            onHover={()=>{}}
            offHover={()=>{}}/>
        </div>
        <div className={Stylus.unStackListColumn}>
          {
            rest.map((item, index) => (
              <ScoreListItem
                border={true}
                number={index + 10}
                data={item.get('ScoreCard').toJS()}
                key={index}
                hover={item.get('hover')}
                onClick={()=>{
                  router.push(`/MainCourse/${index+10}`);
                  closeNav();
                }}
                onHover={toggleHover(index+9)}
                offHover={toggleHover(index+9)}
                numberStyle={index === 1 ? {letterSpacing: '3px'} : {} }/>
            ))
          }
          <ScoreListItem
            border={false}
            number='IN'
            par={
              rest.reduce((acc, item) =>{
                return acc + parseInt(item.get('par'));
              }, 0)
            }
            data={
              rest.reduce((acc, item)=>{
                const i = item.get('ScoreCard').toJS();
                return [acc[0] + i[0] ,acc[1] + i[1] ,acc[2] + i[2] ,acc[3] + i[3] ];
              }, [0,0,0,0])
            }
            hover={false}
            onClick={()=>{}}
            onHover={()=>{}}
            offHover={()=>{}}
            numberStyle={{fontSize: 14}}/>
          <ScoreListItem
            border={false}
            number='TOT'
            data={
              mainCourse.reduce((acc, item)=>{
                const i = item.get('ScoreCard').toJS();
                return [acc[0] + i[0] ,acc[1] + i[1] ,acc[2] + i[2] ,acc[3] + i[3] ];
              }, [0,0,0,0])
            }
            par={
              mainCourse.reduce((acc, item) =>{
                return acc + parseInt(item.get('par'));
              }, 0)
            }
            hover={false}
            onClick={()=>{}}
            onHover={()=>{}}
            offHover={()=>{}}
            numberStyle={{fontSize: 14}}/>
        </div>
      </div>
    );
  }
};

const mapStateToProps = ({Nav, MainCourse}) => {
  return {
    active: Nav.getIn(['mainCourse', 'open']),
    mobileMap: Nav.getIn(['mainCourse', 'mobileMap']),
    mainCourse: MainCourse,
    first: MainCourse.slice(0,9),
    rest: MainCourse.slice(9,18)
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    toggleHover(hole){
      return ()=>{
        dispatch({
          type: 'MAINCOURSE_HOVER_TOGGLE',
          hole: hole
        });
      };
    },
    navToggle(button){
      dispatch({
          type: 'NAV_TOGGLE',
          button: button
        });
    },
    closeNav(){
      dispatch({type:'NAV_CLOSE_ALL'});
    }
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Radium(MainCourse)));
