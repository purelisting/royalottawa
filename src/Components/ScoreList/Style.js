module.exports = {
  base: {
    flexDirection: 'column',
    margin: 'auto',
    maxWidth: '365px',
    width: '100%',
    alignItems: 'center'
  }
};
