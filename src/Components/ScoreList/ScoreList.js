import React, {Component} from 'react';
import Radium, {StyleRoot} from 'radium';
import {connect} from 'react-redux';

import ScoreListItem from '../ScoreListItem/ScoreListItem';

// import Styles
import Style from './Style';

class ScoreList extends Component {
  render(){
    const {list, toggleHover, offset} = this.props;
    return (
      <row style={[Style.base]}>

      </row>
    );
  }
}

ScoreList.defaultProps = {
  offset: 0
};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    toggleHover(hole){
      return ()=>{
        dispatch({
          type: 'MAINCOURSE_HOVER_TOGGLE',
          hole: hole
        });
      };
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Radium(ScoreList));
