import React, {Component} from 'react';
import Radium, {StyleRoot} from 'radium';
import MediaQuery from 'react-responsive';
import {Link} from 'react-router';
import {connect} from 'react-redux';

import Style from './Style';

class Nav extends Component {
  render(){
    const {mainCourse, royal9, propertyMap, hoverMainCourse, hoverRoyal9, hoverPropertyMap, navToggle, closeAll} = this.props;
    return (
      <StyleRoot className='Nav' style={Style.base} key={0}>
        <MediaQuery maxDeviceWidth={767} key={'mobile'}>
          <div style={{
            display: 'block',
            width: '100%',
            height: '50px',
            borderBottom: '1px solid black',
            flexDirection: 'row'
          }}>
            <div style={{
              float: 'left',
              width: '40%',
              height: '50px'
            }}>
              <div style={[Style.button, (mainCourse || ( !royal9 && !propertyMap && hoverMainCourse)) && Style.button.active, {
                width: '100%',
                height: '50px',
                lineHeight: '50px',
                margin: 'auto',
                borderLeft: 'none',
                borderTop: 'none',
                borderRight: 'none'
              }]} onClick={navToggle('mainCourse')} key={'main'}>
                MAIN
              </div>
            </div>
            <div style={{
              float: 'left',
              width: '20%',
              height: '50px',
              borderLeft: '1px solid black',
              borderRight: '1px solid black'
            }}>
              <Link to='/'>
                <img style={{
                  display: 'block',
                  margin: 'auto',
                  width: 'auto',
                  maxHeight: '100%',
                  padding: '2px 10px',
                  boxSizing: 'border-box'
                }} src='/img/Logo.png' />
              </Link>
            </div>
            <div style={{
              float: 'left',
              width: '40%',
              height: '50px'
            }}>
              <div style={[Style.button, (royal9 || (!mainCourse && !propertyMap && hoverRoyal9)) && Style.button.active, {
                width: '100%',
                height: '50px',
                lineHeight: '50px',
                margin: 'auto',
                borderTop: 'none',
                borderLeft: 'none',
                borderRight: 'none'
              }]} onClick={navToggle('royal9')} key={'royal9'}>
                ROYAL 9
              </div>
            </div>
          </div>
          {/*<row style={[Style.row, {
            }]}>
            <column cols='5' style={Style.column}>
              <div style={[Style.button, (mainCourse || ( !royal9 && !propertyMap && hoverMainCourse)) && Style.button.active, {
                width: '100%',
                margin: 'auto',
                borderLeft: 'none',
                borderTop: 'none',
                borderRight: 'none'
            }]} onClick={navToggle('mainCourse')} key={'main'}>
                MAIN
              </div>
            </column>
            <column cols='2' style={[Style.column, {width: '50px', display: 'block'}]}>
              <a href='/' style={[Style.button, {
                display: 'block',
                width: '100%',
                margin: 'auto',
                borderTop: 'none',
                padding: '5px 0px'
              }]} key={'property'}>
                <img style={{width: '50%'}} src='/img/Logo.png' />
              </a>
            </column>
            <column cols='5' style={Style.column}>
              <div style={[Style.button, (royal9 || (!mainCourse && !propertyMap && hoverRoyal9)) && Style.button.active, {
                width: '100%',
                margin: 'auto',
                borderTop: 'none',
                borderLeft: 'none',
                borderRight: 'none'
              }]} onClick={navToggle('royal9')} key={'royal9'}>
                ROYAL 9
              </div>
            </column>
            {/*<column cols='4' style={Style.column}>
              <div style={[Style.button, (propertyMap || (!mainCourse && !royal9 && hoverPropertyMap)) && Style.button.active, {
                width: '100%',
                margin: 'auto',
                borderLeft: 'none',
                borderTop: 'none',
                borderRight: 'none'
              }]} onClick={navToggle('propertyMap')} key={'property'}>
                PROPERTY
              </div>
            </column>
          </row>*/}
        </MediaQuery>
        <MediaQuery minDeviceWidth={768} key={'tablet'}>
          <row style={[Style.row, {maxWidth: '600px'}]}>
            <column cols='4' style={Style.column}>
              <div style={[Style.button, (mainCourse || ( !royal9 && !propertyMap && hoverMainCourse)) && Style.button.active]} onClick={navToggle('mainCourse')} key={'main Course'}>
                MAIN COURSE
              </div>
            </column>
            <column cols='4' style={Style.column}>
              <div style={[Style.button, (royal9 || (!mainCourse && !propertyMap && hoverRoyal9)) && Style.button.active]} onClick={navToggle('royal9')} key={'royal 9'}>
                ROYAL 9
              </div>
            </column>
            <column cols='4' style={Style.column}>
              <div style={[Style.button, (propertyMap || (!mainCourse && !royal9 && hoverPropertyMap)) && Style.button.active]} onClick={navToggle('propertyMap')} key={'property map'}>
                PROPERTY MAP
              </div>
            </column>
            <i className='btr bt-times' style={[Style.close, !mainCourse && !royal9 && !propertyMap && Style.close.hide]} onClick={closeAll()} key={'closeAll'} />
          </row>
        </MediaQuery>
      </StyleRoot>
    );
  }
}

const mapStateToProps = ({Nav})=>{
  return {
    mainCourse: Nav.getIn(['mainCourse', 'open']),
    hoverMainCourse: Nav.getIn(['mainCourse', 'hover']),
    royal9: Nav.getIn(['royal9', 'open']),
    hoverRoyal9: Nav.getIn(['royal9', 'hover']),
    propertyMap: Nav.getIn(['propertyMap', 'open']),
    hoverPropertyMap: Nav.getIn(['propertyMap', 'hover'])
  };
};

const mapDispatchToProps = (dispatch)=>{
  return {
    navToggle(button){
      return ()=>{
        dispatch({
          type: 'NAV_TOGGLE',
          button: button
        });
      };
    },
    closeAll(){
      return ()=>{
        dispatch({type: 'NAV_CLOSE_ALL'});
      };
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Radium(Nav));
