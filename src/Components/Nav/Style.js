import Color from 'color';
import {font, color} from '../../Variables';

const {white, black, gold} = color;

export default {
  base: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    backgroundColor: Color(white).rgbString(),
    zIndex: 10000,
    boxShadow: 'none',
    '@media (min-width: 768px)':{
      boxShadow: `0px 1px 3px 0px ${Color(black).clearer(0.64).rgbString()}`
    }
  },
  row:{
    margin: 'auto'
  },
  column: {
    margin: 'auto'
  },
  button: {
    borderLeft: `1px solid ${Color(black).rgbString()}`,
    borderRight: `1px solid ${Color(black).rgbString()}`,
    borderTop: `1px solid ${Color(black).rgbString()}`,
    borderBottom: `1px solid ${Color(black).rgbString()}`,
    width: 190,
    height: 40,
    lineHeight: '40px',
    fontFamily: font.head,
    fontWeight: 400,
    cursor: 'pointer',
    color: Color(black).rgbString(),
    textAlign: 'center',
    fontSize: 14,
    letterSpacing: '3px',
    margin: '10px auto',
    ':hover':{
      backgroundColor: Color('#EEE1B5').rgbString()
    },
    active:{
      backgroundColor: Color('#EEE1B5').rgbString()
    }
  },
  close: {
    position: 'absolute',
    top: '50%',
    right: 20,
    transform: 'translate(0%, -50%)',
    cursor: 'pointer',
    color: Color(black).rgbString(),
    fontSize: 20,
    ':hover':{
      color: 'grey'
    },
    hide: {
      display: 'none'
    }
  }
};
