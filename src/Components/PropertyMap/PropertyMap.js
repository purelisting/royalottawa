import React, {Component} from 'react';
import {Motion, spring, presets} from 'react-motion';
import {connect} from 'react-redux';
import MediaQuery from 'react-responsive';

import Logo from '../Logo/Logo';
import Path from '../Path/Path';
import Hand from '../Hand/Hand';

import Stylus from './PropertyMap.styl';

class PropertyMap extends Component {
  render(){
    const {active, switchCourse} = this.props;
    return (
      <Motion style={{
          Y: active ? spring(0, presets.gentle) : spring(-100, presets.gentle)
        }}>
        {
          ({Y}) => (
            <div className={Stylus.PropertyMap} style={{
              transform: `translate(0%, ${Y}%)`
            }}>
              <MediaQuery maxDeviceWidth={767}>
                <div />
              </MediaQuery>
              <MediaQuery minDeviceWidth={768} maxDeviceWidth={1024} orientation='portrait'>
                <div className={Stylus.wrapper}>
                  <div className={`${Stylus.column} ${Stylus.Map}`}>
                    <div className={Stylus.imgWrapper}>
                      <img className={Stylus.img} src='/img/PropertyMap.png' />
                      <SVG {...this.props} />
                      <Hand style={{
                        position: 'absolute',
                        bottom: '0px',
                        right: '0px'
                      }} />
                      <div className={`${Stylus.column} ${Stylus.Logo}`}>
                        <Logo />
                      </div>
                    </div>
                  </div>
                </div>
              </MediaQuery>
              <MediaQuery minDeviceWidth={768} maxDeviceWidth={1024} orientation='landscape'>
                <div className={Stylus.wrapper}>
                  <div className={`${Stylus.column} ${Stylus.Logo}`}>
                    <div className={Stylus.LeftContainer}>
                      <Logo />
                      <Hand style={{
                        margin: '40px auto'
                      }} />
                    </div>
                  </div>
                  <div className={`${Stylus.column} ${Stylus.Map}`}>
                    <div className={Stylus.imgWrapper}>
                      <img className={Stylus.img} src='/img/PropertyMap.png' />
                      <SVG {...this.props} />
                    </div>
                  </div>
                </div>
              </MediaQuery>
              <MediaQuery minDeviceWidth={1025}>
                <div className={Stylus.wrapper}>
                  <div className={`${Stylus.column} ${Stylus.Logo}`}>
                    <div className={Stylus.LeftContainer}>
                      <Logo />
                      <Hand style={{
                        margin: '40px auto'
                      }} />
                    </div>
                  </div>
                  <div className={`${Stylus.column} ${Stylus.Map}`}>
                    <div className={Stylus.imgWrapper}>
                      <img className={Stylus.img} src='/img/PropertyMap.png' />
                      <SVG {...this.props} />
                    </div>
                  </div>
                </div>
              </MediaQuery>
            </div>
          )
        }
      </Motion>
    );
  }
};

class SVG extends Component {
  constructor(props){
    super(props);
    this.state = {
      hover: [false, false]
    };
  }
  toggleHover(index){
    return ()=>{
      this.setState({
        hover: this.state.hover.map((i, key) => key === index && !i)
      });
    };
  }
  render(){
    const {active, switchCourse} = this.props;
    const {hover} = this.state;
    return (
      <div className={Stylus.SVG}>
        <svg id="Layer_1" x="0px" y="0px" viewBox="0 0 765 837">
          <g id="Layer_x0020_1_1_">
            {
              [
                {
                  onClick: switchCourse('mainCourse'),
                  d: `M148.3,181.7c17.8-2.7,35.9-7.2,53.9-8.4c20.1-1.4,41,1.4,61.5,0.4c12.1-0.6,24-2.3,36.1-2.9
                    c12.3-0.6,24.6-0.6,37.1-1c11.9-0.4,29.7,1.8,27.7-15.6c-2.3-21.3,19.3-11.3,33.2-7.2c16.6,4.9,32.8,17.6,49.8,5.3
                    c10.1-7.4,7.6-23.2,18.9-31.8c12.1-9.2,20.3-9,20.7,8.6c0.2,14.4,1.2,32.6,4.3,46.8c2.7,11.9,5.7,23.6,8.2,35.5
                    c3.1,14.6,4.9,29.5,7.2,44.3c1.8,10.9,3.9,21.7,5.9,32.6c3.5,19.9,7.6,38.1,13.1,57.6c4.1,14.6,9.6,28.7,11.1,41.2
                    c1.6,11.5,7.2,21.5,9.2,32.8c1.2,6.4,2,13.1,2.5,19.5c0.4,4.5,0.6,9.2,0.2,13.7c-0.8,7.8-3.5,16.4-4.9,24.2
                    c-1.2,6.4-2.1,11.1-1.4,17.6c0.6,4.1,2.5,12.3-0.4,15.6c-2.5,2.9-6.6,4.3-10.3,5.5c-5.9,1.8-16.2-1.2-19.5,6
                    c-0.8,2,0.6,4.3,1.4,6.2c1.8,4.5,4.7,6.6,4.5,11.9c-0.2,4.3-0.2,9.6,2.7,13.7c7.6,10.3,18.7,1.2,29.3-2.3
                    c7.6-2.5,11.5-5.3,19.5-1.6c7.8,3.5,6.4,8.2,11.1,13.3c2.3,2.5,8.2,1.6,11.3,1.4c10.3-0.8,16,11.7,11.1,19.9
                    c-1.6,2.5-4.1,4.9-7.6,7.4c-3.9,2.7-6.2,4.3-10.3,6.8c-2,1.4-2,4.5-3.1,6.4c-4.1,6.6-5.9,7.8-13.1,10c-4.1,1.2-8.2,1.6-12.1,3.1
                    c-4.1,1.8-7.4,4.9-11.5,7c-12.3,6.4-25,12.5-37.3,18.9c-23,11.9-45.7,24-68.9,35.1c-11.1,5.5-22.2,10.9-33.4,16.2
                    c-10.7,5.1-21.1,10.7-32.6,13.5c-17,3.7-46.8-1.8-57,15c-7.6,12.3-17,18.3-28.5,26.1c-10.1,6.8-20.1,13.7-30.2,20.1
                    c-17.2,10.9-35.1,22.6-53.7,31.4c-12.3,5.9-23,1.8-27.9-10.9c-5.3-14,0.4-20.9,2-34.9c0.8-7-4.9-11.1-2.1-21.1
                    c1.6-5.7,4.9-10.7,5.5-16.6c1.4-14.4-4.1-17.2-7.4-28.3c-2.3-7.4-1.2-15.2-1.6-22.8c-0.4-8,5.5-16.4,5.5-24.4
                    c0.2-15-4.3-32,0.4-46.6c1.8-5.3,3.9-19.3,3.7-25c-0.4-10.3-12.7-10.1-19.9-13.1c-12.9-5.1-18.1-8.8-27.5-18.9
                    c-7.8-8.4-10.3-22.2-17.6-31c-7.6-9.2-19.7-13.1-27.1-22.6c-5.9-7.6-12.1-14.8-17.6-22.8c-5.5-8.2-3.9-16.6-8.2-25.6
                    c-2.9-6.4-11.7-21.7-13.1-28.5c-2.9-15.2-2-24.4-2.7-39.8c-0.6-9.8-3.9-19.5-3.5-29.5c0.6-13.5,8.2-33.6,3.1-46.4
                    c-2.9-7.4-10.5-12.1-15.4-18.3C24.3,264,18.7,251.3,14,239c-4.7-11.9,1.2-17,9.4-24.6c8.8-8-0.8-19.1-4.5-27.1
                    c-4.1-9,0.6-17.4,9.6-21.9c8.2-3.9,23-3.3,31.6-1.4c27.3,6.2,41.4,23.2,70.6,20.7c6-0.6,11.9-1.8,17.8-2.7L148.3,181.7z`
                },
                {
                  onClick: switchCourse('royal9'),
                  d: `M651.3,371.1c3.1,1,8-0.4,10.1-2.9c2.7-3.1,3.3-7.6,3.9-11.7c0.6-5.1-1-10.3-0.4-15.6
                    c1.2-12.9,5.5-25.4,6.2-38.4c0.8-11.9,0.6-27.1-1-39c-1.2-9.2-4.3-18.1-5.9-27.3c-1.6-8.4-2.3-16.8-3.3-25.4
                    c-2-16-2.5-22.8-10.1-37.1c-10-18.5-17.4-38.2-27.3-56.6c-16.6-30.4-41.4-40-71-51.7c-15.6-6-42-11.7-58-3.3
                    c-13.3,6.8-13.5,14.4-30.6,11.1c-17.4-3.3-34.1-11.7-50.7-17.6c-13.7-4.7-27.5-9.2-41.4-13.7c-13.1-4.3-25.8-9-39-12.7
                    c-11.9-3.3-23.8-6.6-36.1-8.8c-20.1-3.5-40.6-4.9-60.9-3.3c-14.8,1.2-29,4.9-43.8,6.3c-18.1,2-35.3-13.5-52.7-8.8
                    c-19.7,5.3-24.2,32.6-26.7,49.2c-3.7,23.6-4.7,45.1-3.3,69.1c1.8,32.6,2,54.1,38.8,48.2c17.8-2.7,35.9-7.2,53.9-8.4
                    c20.1-1.4,41,1.4,61.5,0.4c12.1-0.6,24-2.3,36.1-2.9c12.3-0.6,24.6-0.6,37.1-1c11.9-0.4,29.7,1.8,27.7-15.6
                    c-2.3-21.3,19.3-11.3,33.2-7.2c16.6,4.9,32.8,17.6,49.8,5.3c10.1-7.4,7.6-23.2,18.9-31.8c12.1-9.2,20.3-9,20.7,8.6
                    c0.2,14.4,1.2,32.6,4.3,46.8c2.7,11.9,5.7,23.6,8.2,35.5c3.1,14.6,4.9,29.5,7.2,44.3c1.8,10.9,3.9,21.7,5.9,32.6
                    c3.5,19.9,7.6,38.1,13.1,57.6c4.1,14.6,6.8,26.1,16,38.4c5.9,8,20.5,18.5,29.3,7.2c7.4-9.4-4.5-15,13.7-16.6
                    c9.8-0.8,17.2-6.6,23.4-14c5.5-6.6,4.7-12.7,16-12.7c13.3,0,13.1,6,19.9,14.6c2.5,3.3,3.7,7,7.8,8.2L651.3,371.1z`
                }
              ].map(({d, onClick}, index)=>(
                <Path
                  onClick={onClick}
                  d={d}
                  key={index}
                  hover={hover[index]}
                  onHover={this.toggleHover(index)}
                  offHover={this.toggleHover(index)}/>
              ))
            }
          </g>
          <rect x="0.2" y="0.2" className="st0" width="764.7" height="836.6" style={{display: `none`}}/>
        </svg>
      </div>
    );
  }
};


const mapStateToProps = ({Nav}) => {
  return {
    active: Nav.getIn(['propertyMap', 'open'])
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    switchCourse(button){
      return ()=>{
        dispatch({
          type: 'NAV_TOGGLE',
          button: button
        });
      };
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PropertyMap);
