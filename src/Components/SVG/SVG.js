import React, {Component} from 'react';
import Radium from 'radium';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';

import Path from '../Path/Path';

// import Styles
import Style from './SVG.styl';

class SVG extends Component {
  render(){
    const {path, viewBox, toggleHover, width, height, router, linkTo, closeNav} = this.props;
    return (
      <div className={Style.SVG}>
        <svg x="0px" y="0px" viewBox={viewBox} >
          <g >
            {
              path.map((hole, index)=>(
                <Path
                  onClick={()=>{
                    router.push(`/${linkTo}/${index+1}`);
                    closeNav();
                  }}
                  d={hole.get('path')}
                  key={index}
                  hover={hole.get('hover')}
                  onHover={toggleHover(index)}
                  offHover={toggleHover(index)}/>
              ))
            }
          </g>
          {/*<rect x="0" width={width} height={height} style={{display: `none`}}/>*/}
        </svg>
      </div>
    );
  }
}

const mapStateToProps = ({})=>{
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    closeNav(){
      dispatch({type: 'NAV_CLOSE_ALL'});
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Radium(SVG)));
