import React, {Component} from 'react';
import Color from 'color';
import {color} from '../../Variables';
import {connect} from 'react-redux';
import Radium, {StyleRoot} from 'radium';
import MediaQuery from 'react-responsive';
import {Motion, spring, presets} from 'react-motion';
import {withRouter, Link} from 'react-router';

import ScoreListItem from '../ScoreListItem/ScoreListItem';
import SVG from '../SVG/SVG';
import Hand from '../Hand/Hand';
import Logo from '../Logo/Logo';

// import Styles
// import Style from './Style';
import Stylus from './Royal9.styl';

class Royal9 extends Component {
  render(){
    const {active, mobileMap, royal9, toggleHover, navToggle, router, closeNav} = this.props;
    return (
      <Motion style={{
          Y: active ? spring(0, presets.gentle) : spring(-100, presets.gentle)
        }}>
        {
          ({Y}) => (
            <div className={Stylus.Royal9} style={{
              transform: `translate(0%, ${Y}%)`
            }}>
              <MediaQuery maxDeviceWidth={767}>
                <Motion style={{
                  Y: active ? spring(0, presets.gentle) : spring(-100, presets.gentle)
                }}>
                  {
                    ({Y}) => (
                      <div style={{
                        transform: `translate(0%, ${Y}%)`,
                        width: '100%',
                        height: 230,
                        position: 'fixed',
                        top: 0,
                        left: 0,
                        paddingTop: 50,
                        backgroundColor: Color(color.black).clearer(0.25).rgbString()
                      }}>
                        <div>
                          <div style={{
                            display: 'block',
                            borderBottom: `1px solid ${Color(color.white).rgbString()}`,
                            width: '100%',
                            height: 60,
                            color: Color(color.white).rgbString()
                          }}>
                            <div style={{
                              float: 'left',
                              width: '75%',
                              height: 40,
                              borderRight: `1px solid ${Color(color.white).rgbString()}`,
                              textAlign: 'center',
                              margin: '10px auto',
                              padding: '10px 0px'
                            }} onClick={()=>{
                              router.push(`/Royal9/ScoreCard`);
                              closeNav('royal9');
                            }}>
                              <div style={{
                                fontSize: '16px',
                                fontFamily: 'trajan-pro-3',
                                fontWeight: 400,
                                letterSpacing: '3px'
                              }}>
                                SCORE CARD
                              </div>
                            </div>
                            <div style={{
                              float: 'left',
                              width: '25%',
                              height: 40,
                              textAlign: 'center',
                              margin: '10px auto',
                              padding: '10px 0px'
                            }} onClick={()=>{
                              closeNav('royal9');
                            }}>
                              <i className='btr bt-times' style={{
                                fontSize: 20
                              }}/>
                            </div>
                          </div>
                        </div>
                        <div style={{
                          padding: '20px 0px',
                          flexDirection: 'column',
                          width: '100%',
                          maxWidth: 300,
                          margin: 'auto'
                        }}>
                          {
                            [1,2,3,4,5,6,7,8,9,'','',''].map((hole, index) => (
                              <div className={Stylus.mobileColumn} key={index} onClick={()=>{
                                router.push(`/Royal9/${hole}`);
                                closeNav();
                              }} style={[ ((index+1)%6 === 0) && {borderRight: 'none'} ]}>
                                {hole}
                              </div>
                            ))
                          }
                        </div>
                      </div>
                    )
                  }
                </Motion>
              </MediaQuery>
              <MediaQuery minDeviceWidth={768} maxDeviceWidth={1024} orientation='portrait' >
                <div className={Stylus.wrapper}>
                  <div className={Stylus.Left}>
                    <div className={Stylus.LeftContainer}>
                      <div className={Stylus.oneThird}>
                        <Logo style={{
                          position: 'absolute',
                          top: '50%',
                          left: '50%',
                          transform: 'translate(-50%, -50%)',
                          width: 150,
                          margin: 'auto'
                        }} />
                      </div>
                      <div className={Stylus.oneThird}>
                        <Link to='/Royal9/ScoreCard' style={{
                          position: 'absolute',
                          top: '50%',
                          left: '50%',
                          transform: 'translate(-50%, -50%)',
                          width: 190,
                          height: 40,
                          fontSize: '14px',
                          fontWeight: 400,
                          border: '1px solid black',
                          textAlign: 'center',
                          lineHeight: '40px',
                          letterSpacing: '3px',
                          fontFamily: 'trajan-pro-3',
                          textDecoration: 'none',
                          color: 'black'
                        }} onClick={()=>{
                          closeNav();
                        }}>
                          SCORE CARD
                        </Link>
                      </div>
                      <div className={Stylus.oneThird}>
                        <Hand text='SELECT <br/> HOLE' style={{
                          position: 'absolute',
                          top: '50%',
                          left: '50%',
                          transform: 'translate(-50%, -50%)',
                          width: 150
                        }} />
                      </div>
                    </div>
                  </div>
                  <div className={Stylus.Right}>
                    <div className={Stylus.imgWrapper} style={{
                      maxWidth: 513,
                      maxHeight: 600
                    }}>
                      <img className={Stylus.img} src='/img/Royal9/Royal9.png' />
                      <SVG path={royal9} viewBox="0 0 765 520" toggleHover={toggleHover} linkTo='Royal9'/>
                    </div>
                  </div>
                </div>
              </MediaQuery>
              <MediaQuery minDeviceWidth={768} maxDeviceWidth={1024} orientation='landscape' >
                <div className={Stylus.wrapper}>
                  <div className={Stylus.Left}>
                    <div className={Stylus.LeftContainer}>
                      <Logo style={{
                        width: 150,
                        margin: '50px auto'
                      }} />
                      <Link to='/Royal9/ScoreCard' style={{
                        display: 'block',
                        width: 190,
                        height: 40,
                        fontSize: '14px',
                        fontWeight: 400,
                        border: '1px solid black',
                        textAlign: 'center',
                        lineHeight: '40px',
                        letterSpacing: '3px',
                        fontFamily: 'trajan-pro-3',
                        textDecoration: 'none',
                        color: 'black'
                      }} onClick={()=>{
                        closeNav();
                      }}>
                        SCORE CARD
                      </Link>
                      <Hand text='SELECT <br/> HOLE' style={{
                        width: 150,
                        margin: '50px auto'
                      }} />
                    </div>
                  </div>
                  <div className={Stylus.Right}>
                    <div className={Stylus.imgWrapper} style={{
                      maxWidth: 513,
                      maxHeight: 600
                    }}>
                      <img className={Stylus.img} src='/img/Royal9/Royal9.png' />
                      <SVG path={royal9} viewBox="0 0 765 520" toggleHover={toggleHover} linkTo='Royal9'/>
                    </div>
                  </div>
                </div>
              </MediaQuery>
              <MediaQuery minDeviceWidth={1025}>
              <div className={Stylus.wrapper}>
                <div className={`${Stylus.column} ${Stylus.ScoreList}`}>
                  <StackedList {...this.props} />
                </div>
                <div className={`${Stylus.column} ${Stylus.Map}`}>
                  <div className={Stylus.imgWrapper}>
                    <img className={Stylus.img} src='/img/Royal9/Royal9.png' />
                    <SVG path={royal9} viewBox="0 0 765 520" toggleHover={toggleHover} linkTo='Royal9'/>
                    <Logo style={{
                      position: 'absolute',
                      top: '45%',
                      right: '63%',
                      width: 150
                    }} />
                    <Hand text='SELECT <br/> HOLE' style={{
                      position: 'absolute',
                      top: '60%',
                      left: '42%',
                      width: 150
                    }} />
                  </div>
                </div>
              </div>
              </MediaQuery>
            </div>
          )
        }
      </Motion>
    );
  }
};

class StackedList extends Component {
  render(){
    const {active, royal9, toggleHover, router, closeNav} = this.props;
    return (
      <div className={Stylus.StackedList}>
        <ScoreListItem
          border={true}
          number={'Hole'}
          par={'Par'}
          data={['White', 'Green', 'Gold']}
          numberStyle={{fontSize: 14, fontFamily: 'trajan-pro-3'}} />
        {
          royal9.map((item, index) => (
            <ScoreListItem
              border={true}
              number={index + 1}
              par={item.get('par')}
              data={item.get('ScoreCard').toJS()}
              key={index}
              hover={item.get('hover')}
              onClick={()=>{
                router.push(`/Royal9/${index+1}`);
                closeNav();
              }}
              onHover={toggleHover(index)}
              offHover={toggleHover(index)}/>
          ))
        }
        <ScoreListItem
          border={false}
          number='TOT'
          data={
            royal9.reduce((acc, item)=>{
              const i = item.get('ScoreCard').toJS();
              return [acc[0] + i[0] ,acc[1] + i[1] ,acc[2] + i[2] ];
            }, [0,0,0])
          }
          par={
            royal9.reduce((acc, item) =>{
              return acc + parseInt(item.get('par'));
            }, 0)
          }
          hover={false}
          onClick={()=>{}}
          onHover={()=>{}}
          offHover={()=>{}}
          numberStyle={{fontSize: 14}}/>
      </div>
    );
  }
};

const mapStateToProps = ({Nav, Royal9}) => {
  // console.log(Royal9.toJS());
  return {
    active: Nav.getIn(['royal9', 'open']),
    mobileMap: Nav.getIn(['royal9', 'mobileMap']),
    royal9: Royal9,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    toggleHover(hole){
      return ()=>{
        dispatch({
          type: 'ROYAL9_HOVER_TOGGLE',
          hole: hole
        });
      };
    },
    navToggle(button){
      dispatch({
          type: 'NAV_TOGGLE',
          button: button
        });
    },
    closeNav(){
      dispatch({type:'NAV_CLOSE_ALL'});
    }
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Radium(Royal9)));
