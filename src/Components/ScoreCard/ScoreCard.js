import React, {Component} from 'react';
import Radium, {StyleRoot} from 'radium';

// import Styles
import Stylus from './ScoreCard.styl'
// import Style from './Style';

class ScoreCard extends Component {
  render(){
    const {number, stats} = this.props;
    if(stats.length === 4)
      return (
        <div className={`${Stylus.ScoreCard} ${stats.length === 4 && Stylus.main} ${stats.length === 3 && Stylus.royal9}`}>
          <div>
            <div className={`${Stylus.column} ${stats.length === 4 ? 'main' : 'royal9'} ${Stylus.black}`}>
              <div className='number'>
                {stats[0]}
              </div>
              <div className='name'>
                Yards
              </div>
            </div>
            <div className={`${Stylus.column} ${stats.length === 4 ? 'main' : 'royal9'} ${Stylus.white}`}>
              <div className='number'>
                {stats[1]}
              </div>
              <div className='name'>
                Yards
              </div>
            </div>
            <div className={`${Stylus.column} ${stats.length === 4 ? 'main' : 'royal9'} ${Stylus.green}`}>
              <div className='number'>
                {stats[2]}
              </div>
              <div className='name'>
                Yards
              </div>
            </div>
            {
              stats.length === 4 &&
              <div className={`${Stylus.column} ${stats.length === 4 ? 'main' : 'royal9'} ${Stylus.gold}`}>
                <div className='number'>
                  {stats[3]}
                </div>
                <div className='name'>
                  Yards
                </div>
              </div>
            }
          </div>
        </div>
      );

    return (
      <div className={`${Stylus.ScoreCard} ${stats.length === 4 && Stylus.main} ${stats.length === 3 && Stylus.royal9}`}>
        <div>
          <div className={`${Stylus.column} ${stats.length === 4 ? 'main' : 'royal9'} ${Stylus.white}`}>
            <div className='number'>
              {stats[0]}
            </div>
            <div className='name'>
              Yards
            </div>
          </div>
          <div className={`${Stylus.column} ${stats.length === 4 ? 'main' : 'royal9'} ${Stylus.green}`}>
            <div className='number'>
              {stats[1]}
            </div>
            <div className='name'>
              Yards
            </div>
          </div>
          <div className={`${Stylus.column} ${stats.length === 4 ? 'main' : 'royal9'} ${Stylus.gold}`}>
            <div className='number'>
              {stats[2]}
            </div>
            <div className='name'>
              Yards
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ScoreCard;
