import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';

import Style from './ScoreCardButton.styl';

class ScoreCardButton extends Component {
  render(){
    const {to, closeNav, className, style} = this.props;
    return (
      <Link to={to} className={`${Style.base} ${className}`} style={style} onClick={()=>{
        closeNav();
      }}>
        <i className={`${Style.icon} fa fa-list`} />
        <div className={Style.text}>
          SCORECARD
        </div>
      </Link>
    );
  }
};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    closeNav(){
      dispatch({type: 'NAV_CLOSE_ALL'});
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ScoreCardButton);
