import React, {Component} from 'react';
import Radium, {StyleRoot} from 'radium';
import {Link} from 'react-router';
import MediaQuery from 'react-responsive';
import {Motion, spring, presets} from 'react-motion';

//import Styles
import Stylus from './Page.styl';

//import Components
import Logo from '../Logo/Logo';
import Name from '../Name/Name';
import Paragraph from '../Paragraph/Paragraph';
import ScoreCard from '../ScoreCard/ScoreCard';
import Hole from '../Hole/Hole';
import MainCourse from '../MainCourse/MainCourse';
import Royal9 from '../Royal9/Royal9';
import PropertyMap from '../PropertyMap/PropertyMap';

class Page extends Component {
  constructor(props){
    super(props);
  }
  render(){
    const {number, name, par, scoreCard, img, paragraph, previews, leftLink, rightLink} = this.props;
    const date = new Date();
    const year = date.getFullYear();
    return (
      <Motion defaultStyle={{opacity: 0}} style={{ opacity: spring(1, {stiffness: 60, damping: 20})}}>
        {
          ({opacity}) => (
            <div style={{opacity: opacity}}>
              <MediaQuery maxDeviceWidth={767}>
                <div className={Stylus.page}>
                  <Logo />
                  <Name number={number} name={name} par={par} />
                  <ScoreCard stats={scoreCard} />
                  <Hole src={img} previews={previews} />
                  <Paragraph paragraph={paragraph}/>
                  <Link to={leftLink} className={Stylus.LeftArrow}>
                    <img src='/img/ArrowLeft.svg' />
                  </Link>
                  <Link to={rightLink} className={Stylus.RightArrow}>
                    <img src='/img/ArrowRight.svg' />
                  </Link>
                  <div style={{
                    display: 'block',
                    position: 'absolute',
                    bottom: '10px',
                    left: '50%',
                    transform: 'translate(-50%, 0%)',
                    fontSize: '12px',
                    textAlign: 'center',
                    width: '100%'
                  }}>
                    Copyright © {year} <a style={{
                      textDecoration: 'none'
                    }} href='http://t2greengolf.ca' target="_blank">POWERED BY T2GREEN</a>
                  </div>
                </div>
              </MediaQuery>
              <MediaQuery minDeviceWidth={768}>
                <div className={Stylus.page}>
                  <div className={Stylus.container}>
                    <div className={`${Stylus.column} ${Stylus.Left}`}>
                      <div className={Stylus.wrapper}>
                        <div>
                          <Logo />
                        </div>
                        <div>
                          <Name number={number} name={name} par={par} />
                        </div>
                        <div>
                          <ScoreCard stats={scoreCard} />
                        </div>
                        <div>
                          <Paragraph paragraph={paragraph}/>
                        </div>
                      </div>
                    </div>
                    <div className={`${Stylus.column} ${Stylus.Right}`}>
                      <Hole src={img} previews={previews} />
                    </div>
                  </div>
                  <Link to={leftLink} className={Stylus.LeftArrow}>
                    <img src='/img/ArrowLeft.svg' />
                  </Link>
                  <Link to={rightLink} className={Stylus.RightArrow}>
                    <img src='/img/ArrowRight.svg' />
                  </Link>
                  <div style={{
                    display: 'block',
                    position: 'absolute',
                    bottom: '10px',
                    left: '50%',
                    transform: 'translate(-50%, 0%)',
                    fontSize: '12px',
                    textAlign: 'center',
                    width: '100%'
                  }}>
                    Copyright © {year} <a style={{
                      textDecoration: 'none'
                    }} href='http://t2greengolf.ca' target="_blank">POWERED BY T2GREEN</a>
                  </div>
                </div>
              </MediaQuery>
            </div>
          )
        }
      </Motion>
    );
  }
}

export default Radium(Page);
