import React, {Component} from 'react';

import Style from './Hand.styl';

class Hand extends Component {
  render(){
    const {text, white} = this.props;

    return (
      <div className={Style.Hand} style={this.props.style}>
        <img className={Style.img} src={white ? '/img/handWhite.svg' : '/img/hand.svg'} />

        <div className={Style.text} dangerouslySetInnerHTML={{__html: text ? text : String("SELECT<br />COURSE") }}></div>
      </div>
    );
  }
}

export default Hand;
