import React from 'react';
import {render} from 'react-dom';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router';

import App from './App';

// Pages
import Home from './Pages/Home';

// Main Course
import MScore from './Pages/MainCourse/ScoreCard/ScoreCard';
import M1 from './Pages/MainCourse/Hole1';
import M2 from './Pages/MainCourse/Hole2';
import M3 from './Pages/MainCourse/Hole3';
import M4 from './Pages/MainCourse/Hole4';
import M5 from './Pages/MainCourse/Hole5';
import M6 from './Pages/MainCourse/Hole6';
import M7 from './Pages/MainCourse/Hole7';
import M8 from './Pages/MainCourse/Hole8';
import M9 from './Pages/MainCourse/Hole9';
import M10 from './Pages/MainCourse/Hole10';
import M11 from './Pages/MainCourse/Hole11';
import M12 from './Pages/MainCourse/Hole12';
import M13 from './Pages/MainCourse/Hole13';
import M14 from './Pages/MainCourse/Hole14';
import M15 from './Pages/MainCourse/Hole15';
import M16 from './Pages/MainCourse/Hole16';
import M17 from './Pages/MainCourse/Hole17';
import M18 from './Pages/MainCourse/Hole18';

// Royal 9
import RScore from './Pages/Royal9/ScoreCard/ScoreCard';
import R1 from './Pages/Royal9/Hole1';
import R2 from './Pages/Royal9/Hole2';
import R3 from './Pages/Royal9/Hole3';
import R4 from './Pages/Royal9/Hole4';
import R5 from './Pages/Royal9/Hole5';
import R6 from './Pages/Royal9/Hole6';
import R7 from './Pages/Royal9/Hole7';
import R8 from './Pages/Royal9/Hole8';
import R9 from './Pages/Royal9/Hole9';


render(
  <Router history={browserHistory}>
    <Route path='/' component={App}>
      <IndexRoute component={Home} />

      <Route path='/:courseName' component={Home} />
      <Route path='/MainCourse/1' component={M1} />
      <Route path='/MainCourse/2' component={M2} />
      <Route path='/MainCourse/3' component={M3} />
      <Route path='/MainCourse/4' component={M4} />
      <Route path='/MainCourse/5' component={M5} />
      <Route path='/MainCourse/6' component={M6} />
      <Route path='/MainCourse/7' component={M7} />
      <Route path='/MainCourse/8' component={M8} />
      <Route path='/MainCourse/9' component={M9} />
      <Route path='/MainCourse/10' component={M10} />
      <Route path='/MainCourse/11' component={M11} />
      <Route path='/MainCourse/12' component={M12} />
      <Route path='/MainCourse/13' component={M13} />
      <Route path='/MainCourse/14' component={M14} />
      <Route path='/MainCourse/15' component={M15} />
      <Route path='/MainCourse/16' component={M16} />
      <Route path='/MainCourse/17' component={M17} />
      <Route path='/MainCourse/18' component={M18} />
      <Route path='/MainCourse/ScoreCard' component={MScore} />

      <Route path='/Royal9/1' component={R1} />
      <Route path='/Royal9/2' component={R2} />
      <Route path='/Royal9/3' component={R3} />
      <Route path='/Royal9/4' component={R4} />
      <Route path='/Royal9/5' component={R5} />
      <Route path='/Royal9/6' component={R6} />
      <Route path='/Royal9/7' component={R7} />
      <Route path='/Royal9/8' component={R8} />
      <Route path='/Royal9/9' component={R9} />
      <Route path='/Royal9/ScoreCard' component={RScore} />

    </Route>
  </Router>,
  document.getElementById('root')
);
