var path = require(`path`);
var webpack = require(`webpack`);

var axis = require('axis');
var jeet = require('jeet');
var rupture = require('rupture');

module.exports = {
  entry: {
    Client: `./src/Client`,
  },
  output: {
    path: path.join(__dirname, `public/js`),
    filename: `bundle.js`
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(`production`)
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress:{
        warnings: false
      }
    }),
    new webpack.optimize.DedupePlugin()
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: [`babel`],
      include: path.join(__dirname, `src`)
    },{
      test: /\.styl$/,
      loader: 'style-loader!css-loader!stylus-loader'
    }]
  },
  stylus: {
    use: [axis(), jeet(), rupture()]
  },
  node: {
    fs: `empty`
  }
};
